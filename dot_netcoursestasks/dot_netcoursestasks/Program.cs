﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.ExceptionServices;
using System.Security.Cryptography.X509Certificates;

namespace master;

internal class Program
{
    static void Main(string[] args)
    {
        int userChooseAction;
        Console.WriteLine("Введите строку");
        string userLine = Console.ReadLine();
        Console.WriteLine("Отлично, строка введена, выберите дейсвтие со строкой.");
        Console.WriteLine("1.Вы можете сложить эту строку с другой введенной вами строкой.\n2.Вы можете подсчитать количество подстрок в строке" +
            "\n3.Вы можете найти указанный вами символ в строке.");
        try
        {
            userChooseAction = int.Parse(Console.ReadLine());
            switch(userChooseAction)
            {
                case 1:
                    AddLine(userLine);
                    ChooseClear();
                    break;
                case 2:
                    LineCount(userLine);
                    ChooseClear();
                    break;
                case 3:
                    Console.WriteLine("Окей, введите ваш символ для поиска");
                    char FindElement = Console.ReadLine()[0];
                    int[] arrayIndexChar = FindChar(userLine, FindElement);
                    for ( int i = 0; i < arrayIndexChar.Length; i++)
                    {
                        Console.WriteLine(arrayIndexChar[i]);
                    }
                    ChooseClear();
                    Console.WriteLine("Хотите ли вы удалить символ со всех индексов в вашей строке ? " +
                            "\tДа/Нет");
                        string removeChar = Console.ReadLine();
                        if (removeChar == "Да" || removeChar == "да")
                        {
                            Console.WriteLine(RemoveChar(userLine, FindElement));
                        }
                        else if (removeChar == "Нет" || removeChar == "нет")
                        {
                            Console.WriteLine("Тогда все...");
                        }
                    ChooseClear();
                    break;
            }
        } catch (FormatException)
        {
            Console.WriteLine("Вы ввели неправильное значение. Введите ваш выбор числом" +
                "\nПример = 3");
        }
    }
    static void AddLine(string userLine)
    {
        Console.WriteLine("Введите вторую строку : ");
        string userLine2 = Console.ReadLine();
        Console.WriteLine($"Вы ввели строку {userLine2}");
        string userAddLine = userLine + " " + userLine2;
        Console.WriteLine($"То, что получилось в итоге : {userAddLine}");
    }
    static void LineCount(string userLine)
    {
        int countSpace = 0;
        foreach(char item in userLine)
        {
            if (item == ' '||item == ','||item == ';')
            {
                countSpace++;
            }
        }
        Console.WriteLine($"Количество подстрок в строке : {countSpace}");
    }
    static int[] FindChar(string userLine, char FindElement)
    {
        int index = 0;
        int i = 0;
        List<int> list = new List<int>();
        while ( i < userLine.Length )
        {
            index = userLine.IndexOf(FindElement, index + 1);
            i = index > -1 ? index : 0;
            if (i == 0)
            {
                break;
            }
            list.Add(i);
            i++;
        };

        int []arrayIndexChar = list.ToArray<int>();
        return arrayIndexChar;
        
    }
    static string RemoveChar(string userLine, char FindElement)
    {
        int i = 0;
        
        foreach(char item in userLine)
        {
            if (item == FindElement)
            {
             userLine = userLine.Replace(item.ToString(), string.Empty);
            }
        }
        return userLine;
    }
    static void ConsoleClear()
    {
        Console.Clear();
    }
    static void ChooseClear()
    {
        Console.WriteLine("Хотите ли вы отчистить консоль ?");
        string consoleclear = Console.ReadLine();
        if (consoleclear == "Да" || consoleclear == "да")
        {
            ConsoleClear();
        }
        else if (consoleclear == "Нет" || consoleclear == "нет")
        {
            Console.WriteLine("Окей.");
        }
        Console.WriteLine($"Количество подстрок в строке : {countSpace}");
    }


    private static int[] FindChar(string userInput, char FindElement)
    {
        int index = 0;
        int count = 0;
        List<int> indexCollection = new List<int>();
        while (count < userInput.Length)
        {
            index = userInput.IndexOf(FindElement, index + 1);
            count = index > -1 ? index : 0;
            if (count == 0)
            {
                break;
            }
            indexCollection.Add(count);
            count++;
        };

        int[] arrayIndexChar = indexCollection.ToArray<int>();
        return arrayIndexChar;

    }


    private static string RemoveChar(string userInput, char removed_el)
    {

        foreach (char item in userInput)
        {
            if (item == removed_el)
            {
                userInput = userInput.Replace(item.ToString(), String.Empty);
            }
        }
        return userInput;
    }


    private static void ConsoleClear()
    {
        Console.Clear();
    }


    private static void ChooseClear()
    {
        Console.WriteLine("Хотите ли вы отчистить консоль ? Да = 1, Нет = 2");
        int consoleclr = int.Parse(Console.ReadLine());
        if (consoleclr == 1)
        {
            ConsoleClear();
        }
        else if (consoleclr == 2)
        {
            Console.WriteLine("Окей.");
        }
    }
    private enum DirectoryAction
    {
        AddLine = 1,
        LineCount = 2,
        FindChar = 3,
        RemoveChar = 4,
        ConsoleClear = 5,
        ChooseClear = 6,
    }
}